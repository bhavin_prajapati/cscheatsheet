/*
 * server/controllers/home.js
 */

'use strict';

function express(req, res) {
  res.render('home/express', {
    hello: 'world'
  });
}

function create(req, res) {
  res.render('home/create', {
    hello: 'world'
  });
}


// Public API
exports.express = express;
exports.create = create;

